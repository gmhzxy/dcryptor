// D-Coder.h
// Defines the DCryptor class for actually encoding the binary and installing the stub
// Douglas "Douggem" Confere 2015
#ifndef DCoder_h__
#define DCoder_h__

#include "PEHeader.h"
#include <winnt.h>
#include <cstdio>
/* Steps to crypting:
	Open file and write everything into a byte buffer
	Make .text and .data readable and writeable through section header
	Encode the .text and .data sections
	Write stub at end of file that decodes .text and data, jumps to original entry point
	Change entry point in header to stub
	Write buffer to disk
*/

namespace DUtils{
	extern BYTE stub[400];

	// Just XOR's the given address range by our turning key.  
	// Needs to be inlined because it goes in our stub bytecode
	__forceinline void EncodeSection(void* sectionAddress, int sectionSize) {
		BYTE* section = (BYTE*)sectionAddress;
		char xorKey[1] = { 0x11 };
		for (int i = 0; i < sectionSize; i++) {
			section[i] ^= xorKey[i % sizeof(xorKey)];
		}
	}

	void Decode();
	// This object takes care of setting everything up and writing the new executable to disk
	class DCryptor {
		IMAGE_DOS_HEADER* Header;
		FILE* File;
		BYTE* FileBuffer;
		int FileSize;		
		char* XorKey;
		int XorKeyLen;
		
	public: 
		DCryptor(char* fileName, const char* xorKey);
		~DCryptor() {
			free(FileBuffer);
		}
		void WriteToDisk(char* fileName = nullptr);
		void Encode();	
	};
}
#endif // D-Coder_h__